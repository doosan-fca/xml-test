﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="14008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Controls" Type="Folder">
			<Item Name="tag_Cluster.ctl" Type="VI" URL="../../xmlfile/tag_Cluster.ctl"/>
			<Item Name="xml_Controller_Info.ctl" Type="VI" URL="../../xmlfile/xml_Controller_Info.ctl"/>
			<Item Name="xml_File_Info.ctl" Type="VI" URL="../../xmlfile/xml_File_Info.ctl"/>
			<Item Name="XML_tag_AI_Voltage.ctl" Type="VI" URL="../../xmlfile/XML_tag_AI_Voltage.ctl"/>
			<Item Name="Card.ctl" Type="VI" URL="../../xmlfile/Card.ctl"/>
		</Item>
		<Item Name="DaqMXTasks" Type="Folder">
			<Item Name="NI 9205" Type="NI-DAQmx Task">
				<Property Name="\0\AI.Max" Type="Str">10</Property>
				<Property Name="\0\AI.MeasType" Type="Str">Voltage</Property>
				<Property Name="\0\AI.Min" Type="Str">-10</Property>
				<Property Name="\0\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\0\AI.Voltage.Units" Type="Str">Volts</Property>
				<Property Name="\0\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\0\Name" Type="Str">NI 9205/Voltage_0</Property>
				<Property Name="\0\PhysicalChanName" Type="Str">cDAQ2Mod1/ai0</Property>
				<Property Name="\1\AI.Max" Type="Str">10</Property>
				<Property Name="\1\AI.MeasType" Type="Str">Voltage</Property>
				<Property Name="\1\AI.Min" Type="Str">-10</Property>
				<Property Name="\1\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\1\AI.Voltage.Units" Type="Str">Volts</Property>
				<Property Name="\1\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\1\Name" Type="Str">NI 9205/Voltage_1</Property>
				<Property Name="\1\PhysicalChanName" Type="Str">cDAQ2Mod1/ai1</Property>
				<Property Name="\10\AI.Max" Type="Str">10</Property>
				<Property Name="\10\AI.MeasType" Type="Str">Voltage</Property>
				<Property Name="\10\AI.Min" Type="Str">-10</Property>
				<Property Name="\10\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\10\AI.Voltage.Units" Type="Str">Volts</Property>
				<Property Name="\10\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\10\Name" Type="Str">NI 9205/Voltage_10</Property>
				<Property Name="\10\PhysicalChanName" Type="Str">cDAQ2Mod1/ai10</Property>
				<Property Name="\11\AI.Max" Type="Str">10</Property>
				<Property Name="\11\AI.MeasType" Type="Str">Voltage</Property>
				<Property Name="\11\AI.Min" Type="Str">-10</Property>
				<Property Name="\11\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\11\AI.Voltage.Units" Type="Str">Volts</Property>
				<Property Name="\11\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\11\Name" Type="Str">NI 9205/Voltage_11</Property>
				<Property Name="\11\PhysicalChanName" Type="Str">cDAQ2Mod1/ai11</Property>
				<Property Name="\12\AI.Max" Type="Str">10</Property>
				<Property Name="\12\AI.MeasType" Type="Str">Voltage</Property>
				<Property Name="\12\AI.Min" Type="Str">-10</Property>
				<Property Name="\12\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\12\AI.Voltage.Units" Type="Str">Volts</Property>
				<Property Name="\12\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\12\Name" Type="Str">NI 9205/Voltage_12</Property>
				<Property Name="\12\PhysicalChanName" Type="Str">cDAQ2Mod1/ai12</Property>
				<Property Name="\13\AI.Max" Type="Str">10</Property>
				<Property Name="\13\AI.MeasType" Type="Str">Voltage</Property>
				<Property Name="\13\AI.Min" Type="Str">-10</Property>
				<Property Name="\13\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\13\AI.Voltage.Units" Type="Str">Volts</Property>
				<Property Name="\13\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\13\Name" Type="Str">NI 9205/Voltage_13</Property>
				<Property Name="\13\PhysicalChanName" Type="Str">cDAQ2Mod1/ai13</Property>
				<Property Name="\14\AI.Max" Type="Str">10</Property>
				<Property Name="\14\AI.MeasType" Type="Str">Voltage</Property>
				<Property Name="\14\AI.Min" Type="Str">-10</Property>
				<Property Name="\14\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\14\AI.Voltage.Units" Type="Str">Volts</Property>
				<Property Name="\14\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\14\Name" Type="Str">NI 9205/Voltage_14</Property>
				<Property Name="\14\PhysicalChanName" Type="Str">cDAQ2Mod1/ai14</Property>
				<Property Name="\15\AI.Max" Type="Str">10</Property>
				<Property Name="\15\AI.MeasType" Type="Str">Voltage</Property>
				<Property Name="\15\AI.Min" Type="Str">-10</Property>
				<Property Name="\15\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\15\AI.Voltage.Units" Type="Str">Volts</Property>
				<Property Name="\15\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\15\Name" Type="Str">NI 9205/Voltage_15</Property>
				<Property Name="\15\PhysicalChanName" Type="Str">cDAQ2Mod1/ai15</Property>
				<Property Name="\16\AI.Max" Type="Str">10</Property>
				<Property Name="\16\AI.MeasType" Type="Str">Voltage</Property>
				<Property Name="\16\AI.Min" Type="Str">-10</Property>
				<Property Name="\16\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\16\AI.Voltage.Units" Type="Str">Volts</Property>
				<Property Name="\16\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\16\Name" Type="Str">NI 9205/Voltage_16</Property>
				<Property Name="\16\PhysicalChanName" Type="Str">cDAQ2Mod1/ai16</Property>
				<Property Name="\17\AI.Max" Type="Str">10</Property>
				<Property Name="\17\AI.MeasType" Type="Str">Voltage</Property>
				<Property Name="\17\AI.Min" Type="Str">-10</Property>
				<Property Name="\17\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\17\AI.Voltage.Units" Type="Str">Volts</Property>
				<Property Name="\17\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\17\Name" Type="Str">NI 9205/Voltage_17</Property>
				<Property Name="\17\PhysicalChanName" Type="Str">cDAQ2Mod1/ai17</Property>
				<Property Name="\18\AI.Max" Type="Str">10</Property>
				<Property Name="\18\AI.MeasType" Type="Str">Voltage</Property>
				<Property Name="\18\AI.Min" Type="Str">-10</Property>
				<Property Name="\18\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\18\AI.Voltage.Units" Type="Str">Volts</Property>
				<Property Name="\18\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\18\Name" Type="Str">NI 9205/Voltage_18</Property>
				<Property Name="\18\PhysicalChanName" Type="Str">cDAQ2Mod1/ai18</Property>
				<Property Name="\19\AI.Max" Type="Str">10</Property>
				<Property Name="\19\AI.MeasType" Type="Str">Voltage</Property>
				<Property Name="\19\AI.Min" Type="Str">-10</Property>
				<Property Name="\19\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\19\AI.Voltage.Units" Type="Str">Volts</Property>
				<Property Name="\19\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\19\Name" Type="Str">NI 9205/Voltage_19</Property>
				<Property Name="\19\PhysicalChanName" Type="Str">cDAQ2Mod1/ai19</Property>
				<Property Name="\2\AI.Max" Type="Str">10</Property>
				<Property Name="\2\AI.MeasType" Type="Str">Voltage</Property>
				<Property Name="\2\AI.Min" Type="Str">-10</Property>
				<Property Name="\2\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\2\AI.Voltage.Units" Type="Str">Volts</Property>
				<Property Name="\2\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\2\Name" Type="Str">NI 9205/Voltage_2</Property>
				<Property Name="\2\PhysicalChanName" Type="Str">cDAQ2Mod1/ai2</Property>
				<Property Name="\20\AI.Max" Type="Str">10</Property>
				<Property Name="\20\AI.MeasType" Type="Str">Voltage</Property>
				<Property Name="\20\AI.Min" Type="Str">-10</Property>
				<Property Name="\20\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\20\AI.Voltage.Units" Type="Str">Volts</Property>
				<Property Name="\20\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\20\Name" Type="Str">NI 9205/Voltage_20</Property>
				<Property Name="\20\PhysicalChanName" Type="Str">cDAQ2Mod1/ai20</Property>
				<Property Name="\21\AI.Max" Type="Str">10</Property>
				<Property Name="\21\AI.MeasType" Type="Str">Voltage</Property>
				<Property Name="\21\AI.Min" Type="Str">-10</Property>
				<Property Name="\21\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\21\AI.Voltage.Units" Type="Str">Volts</Property>
				<Property Name="\21\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\21\Name" Type="Str">NI 9205/Voltage_21</Property>
				<Property Name="\21\PhysicalChanName" Type="Str">cDAQ2Mod1/ai21</Property>
				<Property Name="\22\AI.Max" Type="Str">10</Property>
				<Property Name="\22\AI.MeasType" Type="Str">Voltage</Property>
				<Property Name="\22\AI.Min" Type="Str">-10</Property>
				<Property Name="\22\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\22\AI.Voltage.Units" Type="Str">Volts</Property>
				<Property Name="\22\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\22\Name" Type="Str">NI 9205/Voltage_22</Property>
				<Property Name="\22\PhysicalChanName" Type="Str">cDAQ2Mod1/ai22</Property>
				<Property Name="\23\AI.Max" Type="Str">10</Property>
				<Property Name="\23\AI.MeasType" Type="Str">Voltage</Property>
				<Property Name="\23\AI.Min" Type="Str">-10</Property>
				<Property Name="\23\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\23\AI.Voltage.Units" Type="Str">Volts</Property>
				<Property Name="\23\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\23\Name" Type="Str">NI 9205/Voltage_23</Property>
				<Property Name="\23\PhysicalChanName" Type="Str">cDAQ2Mod1/ai23</Property>
				<Property Name="\24\AI.Max" Type="Str">10</Property>
				<Property Name="\24\AI.MeasType" Type="Str">Voltage</Property>
				<Property Name="\24\AI.Min" Type="Str">-10</Property>
				<Property Name="\24\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\24\AI.Voltage.Units" Type="Str">Volts</Property>
				<Property Name="\24\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\24\Name" Type="Str">NI 9205/Voltage_24</Property>
				<Property Name="\24\PhysicalChanName" Type="Str">cDAQ2Mod1/ai24</Property>
				<Property Name="\25\AI.Max" Type="Str">10</Property>
				<Property Name="\25\AI.MeasType" Type="Str">Voltage</Property>
				<Property Name="\25\AI.Min" Type="Str">-10</Property>
				<Property Name="\25\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\25\AI.Voltage.Units" Type="Str">Volts</Property>
				<Property Name="\25\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\25\Name" Type="Str">NI 9205/Voltage_25</Property>
				<Property Name="\25\PhysicalChanName" Type="Str">cDAQ2Mod1/ai25</Property>
				<Property Name="\26\AI.Max" Type="Str">10</Property>
				<Property Name="\26\AI.MeasType" Type="Str">Voltage</Property>
				<Property Name="\26\AI.Min" Type="Str">-10</Property>
				<Property Name="\26\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\26\AI.Voltage.Units" Type="Str">Volts</Property>
				<Property Name="\26\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\26\Name" Type="Str">NI 9205/Voltage_26</Property>
				<Property Name="\26\PhysicalChanName" Type="Str">cDAQ2Mod1/ai26</Property>
				<Property Name="\27\AI.Max" Type="Str">10</Property>
				<Property Name="\27\AI.MeasType" Type="Str">Voltage</Property>
				<Property Name="\27\AI.Min" Type="Str">-10</Property>
				<Property Name="\27\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\27\AI.Voltage.Units" Type="Str">Volts</Property>
				<Property Name="\27\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\27\Name" Type="Str">NI 9205/Voltage_27</Property>
				<Property Name="\27\PhysicalChanName" Type="Str">cDAQ2Mod1/ai27</Property>
				<Property Name="\28\AI.Max" Type="Str">10</Property>
				<Property Name="\28\AI.MeasType" Type="Str">Voltage</Property>
				<Property Name="\28\AI.Min" Type="Str">-10</Property>
				<Property Name="\28\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\28\AI.Voltage.Units" Type="Str">Volts</Property>
				<Property Name="\28\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\28\Name" Type="Str">NI 9205/Voltage_28</Property>
				<Property Name="\28\PhysicalChanName" Type="Str">cDAQ2Mod1/ai28</Property>
				<Property Name="\29\AI.Max" Type="Str">10</Property>
				<Property Name="\29\AI.MeasType" Type="Str">Voltage</Property>
				<Property Name="\29\AI.Min" Type="Str">-10</Property>
				<Property Name="\29\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\29\AI.Voltage.Units" Type="Str">Volts</Property>
				<Property Name="\29\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\29\Name" Type="Str">NI 9205/Voltage_29</Property>
				<Property Name="\29\PhysicalChanName" Type="Str">cDAQ2Mod1/ai29</Property>
				<Property Name="\3\AI.Max" Type="Str">10</Property>
				<Property Name="\3\AI.MeasType" Type="Str">Voltage</Property>
				<Property Name="\3\AI.Min" Type="Str">-10</Property>
				<Property Name="\3\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\3\AI.Voltage.Units" Type="Str">Volts</Property>
				<Property Name="\3\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\3\Name" Type="Str">NI 9205/Voltage_3</Property>
				<Property Name="\3\PhysicalChanName" Type="Str">cDAQ2Mod1/ai3</Property>
				<Property Name="\30\AI.Max" Type="Str">10</Property>
				<Property Name="\30\AI.MeasType" Type="Str">Voltage</Property>
				<Property Name="\30\AI.Min" Type="Str">-10</Property>
				<Property Name="\30\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\30\AI.Voltage.Units" Type="Str">Volts</Property>
				<Property Name="\30\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\30\Name" Type="Str">NI 9205/Voltage_30</Property>
				<Property Name="\30\PhysicalChanName" Type="Str">cDAQ2Mod1/ai30</Property>
				<Property Name="\31\AI.Max" Type="Str">10</Property>
				<Property Name="\31\AI.MeasType" Type="Str">Voltage</Property>
				<Property Name="\31\AI.Min" Type="Str">-10</Property>
				<Property Name="\31\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\31\AI.Voltage.Units" Type="Str">Volts</Property>
				<Property Name="\31\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\31\Name" Type="Str">NI 9205/Voltage_31</Property>
				<Property Name="\31\PhysicalChanName" Type="Str">cDAQ2Mod1/ai31</Property>
				<Property Name="\4\AI.Max" Type="Str">10</Property>
				<Property Name="\4\AI.MeasType" Type="Str">Voltage</Property>
				<Property Name="\4\AI.Min" Type="Str">-10</Property>
				<Property Name="\4\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\4\AI.Voltage.Units" Type="Str">Volts</Property>
				<Property Name="\4\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\4\Name" Type="Str">NI 9205/Voltage_4</Property>
				<Property Name="\4\PhysicalChanName" Type="Str">cDAQ2Mod1/ai4</Property>
				<Property Name="\5\AI.Max" Type="Str">10</Property>
				<Property Name="\5\AI.MeasType" Type="Str">Voltage</Property>
				<Property Name="\5\AI.Min" Type="Str">-10</Property>
				<Property Name="\5\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\5\AI.Voltage.Units" Type="Str">Volts</Property>
				<Property Name="\5\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\5\Name" Type="Str">NI 9205/Voltage_5</Property>
				<Property Name="\5\PhysicalChanName" Type="Str">cDAQ2Mod1/ai5</Property>
				<Property Name="\6\AI.Max" Type="Str">10</Property>
				<Property Name="\6\AI.MeasType" Type="Str">Voltage</Property>
				<Property Name="\6\AI.Min" Type="Str">-10</Property>
				<Property Name="\6\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\6\AI.Voltage.Units" Type="Str">Volts</Property>
				<Property Name="\6\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\6\Name" Type="Str">NI 9205/Voltage_6</Property>
				<Property Name="\6\PhysicalChanName" Type="Str">cDAQ2Mod1/ai6</Property>
				<Property Name="\7\AI.Max" Type="Str">10</Property>
				<Property Name="\7\AI.MeasType" Type="Str">Voltage</Property>
				<Property Name="\7\AI.Min" Type="Str">-10</Property>
				<Property Name="\7\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\7\AI.Voltage.Units" Type="Str">Volts</Property>
				<Property Name="\7\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\7\Name" Type="Str">NI 9205/Voltage_7</Property>
				<Property Name="\7\PhysicalChanName" Type="Str">cDAQ2Mod1/ai7</Property>
				<Property Name="\8\AI.Max" Type="Str">10</Property>
				<Property Name="\8\AI.MeasType" Type="Str">Voltage</Property>
				<Property Name="\8\AI.Min" Type="Str">-10</Property>
				<Property Name="\8\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\8\AI.Voltage.Units" Type="Str">Volts</Property>
				<Property Name="\8\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\8\Name" Type="Str">NI 9205/Voltage_8</Property>
				<Property Name="\8\PhysicalChanName" Type="Str">cDAQ2Mod1/ai8</Property>
				<Property Name="\9\AI.Max" Type="Str">10</Property>
				<Property Name="\9\AI.MeasType" Type="Str">Voltage</Property>
				<Property Name="\9\AI.Min" Type="Str">-10</Property>
				<Property Name="\9\AI.TermCfg" Type="Str">RSE</Property>
				<Property Name="\9\AI.Voltage.Units" Type="Str">Volts</Property>
				<Property Name="\9\ChanType" Type="Str">Analog Input</Property>
				<Property Name="\9\Name" Type="Str">NI 9205/Voltage_9</Property>
				<Property Name="\9\PhysicalChanName" Type="Str">cDAQ2Mod1/ai9</Property>
				<Property Name="Channels" Type="Str">NI 9205/Voltage_0, NI 9205/Voltage_1, NI 9205/Voltage_2, NI 9205/Voltage_3, NI 9205/Voltage_4, NI 9205/Voltage_5, NI 9205/Voltage_6, NI 9205/Voltage_7, NI 9205/Voltage_8, NI 9205/Voltage_9, NI 9205/Voltage_10, NI 9205/Voltage_11, NI 9205/Voltage_12, NI 9205/Voltage_13, NI 9205/Voltage_14, NI 9205/Voltage_15, NI 9205/Voltage_16, NI 9205/Voltage_17, NI 9205/Voltage_18, NI 9205/Voltage_19, NI 9205/Voltage_20, NI 9205/Voltage_21, NI 9205/Voltage_22, NI 9205/Voltage_23, NI 9205/Voltage_24, NI 9205/Voltage_25, NI 9205/Voltage_26, NI 9205/Voltage_27, NI 9205/Voltage_28, NI 9205/Voltage_29, NI 9205/Voltage_30, NI 9205/Voltage_31</Property>
				<Property Name="Name" Type="Str">NI 9205</Property>
				<Property Name="SampClk.ActiveEdge" Type="Str">Rising</Property>
				<Property Name="SampClk.Rate" Type="Str">1000</Property>
				<Property Name="SampClk.Src" Type="Str"></Property>
				<Property Name="SampQuant.SampMode" Type="Str">Finite Samples</Property>
				<Property Name="SampQuant.SampPerChan" Type="Str">100</Property>
				<Property Name="SampTimingType" Type="Str">Sample Clock</Property>
			</Item>
			<Item Name="NI 9265 x1" Type="NI-DAQmx Task">
				<Property Name="\0\AO.Current.Units" Type="Str">Amps</Property>
				<Property Name="\0\AO.Max" Type="Str">0.02</Property>
				<Property Name="\0\AO.Min" Type="Str">0</Property>
				<Property Name="\0\AO.OutputType" Type="Str">Current</Property>
				<Property Name="\0\ChanType" Type="Str">Analog Output</Property>
				<Property Name="\0\Name" Type="Str">NI 9265 x1/CurrentOut_0</Property>
				<Property Name="\0\PhysicalChanName" Type="Str">cDAQ2Mod5/ao0</Property>
				<Property Name="\1\AO.Current.Units" Type="Str">Amps</Property>
				<Property Name="\1\AO.Max" Type="Str">0.02</Property>
				<Property Name="\1\AO.Min" Type="Str">0</Property>
				<Property Name="\1\AO.OutputType" Type="Str">Current</Property>
				<Property Name="\1\ChanType" Type="Str">Analog Output</Property>
				<Property Name="\1\Name" Type="Str">NI 9265 x1/CurrentOut_1</Property>
				<Property Name="\1\PhysicalChanName" Type="Str">cDAQ2Mod5/ao1</Property>
				<Property Name="\2\AO.Current.Units" Type="Str">Amps</Property>
				<Property Name="\2\AO.Max" Type="Str">0.02</Property>
				<Property Name="\2\AO.Min" Type="Str">0</Property>
				<Property Name="\2\AO.OutputType" Type="Str">Current</Property>
				<Property Name="\2\ChanType" Type="Str">Analog Output</Property>
				<Property Name="\2\Name" Type="Str">NI 9265 x1/CurrentOut_2</Property>
				<Property Name="\2\PhysicalChanName" Type="Str">cDAQ2Mod5/ao2</Property>
				<Property Name="\3\AO.Current.Units" Type="Str">Amps</Property>
				<Property Name="\3\AO.Max" Type="Str">0.02</Property>
				<Property Name="\3\AO.Min" Type="Str">0</Property>
				<Property Name="\3\AO.OutputType" Type="Str">Current</Property>
				<Property Name="\3\ChanType" Type="Str">Analog Output</Property>
				<Property Name="\3\Name" Type="Str">NI 9265 x1/CurrentOut_3</Property>
				<Property Name="\3\PhysicalChanName" Type="Str">cDAQ2Mod5/ao3</Property>
				<Property Name="Channels" Type="Str">NI 9265 x1/CurrentOut_0, NI 9265 x1/CurrentOut_1, NI 9265 x1/CurrentOut_2, NI 9265 x1/CurrentOut_3</Property>
				<Property Name="Name" Type="Str">NI 9265 x1</Property>
				<Property Name="RegenMode" Type="Str">Allow Regeneration</Property>
				<Property Name="SampClk.ActiveEdge" Type="Str">Rising</Property>
				<Property Name="SampClk.Rate" Type="Str">1000</Property>
				<Property Name="SampClk.Src" Type="Str"></Property>
				<Property Name="SampQuant.SampMode" Type="Str">Finite Samples</Property>
				<Property Name="SampQuant.SampPerChan" Type="Str">100</Property>
				<Property Name="SampTimingType" Type="Str">Sample Clock</Property>
			</Item>
			<Item Name="NI 9265 x2" Type="NI-DAQmx Task">
				<Property Name="\0\AO.Current.Units" Type="Str">Amps</Property>
				<Property Name="\0\AO.Max" Type="Str">0.02</Property>
				<Property Name="\0\AO.Min" Type="Str">0</Property>
				<Property Name="\0\AO.OutputType" Type="Str">Current</Property>
				<Property Name="\0\ChanType" Type="Str">Analog Output</Property>
				<Property Name="\0\Name" Type="Str">NI 9265 x2/CurrentOut_0</Property>
				<Property Name="\0\PhysicalChanName" Type="Str">cDAQ2Mod6/ao0</Property>
				<Property Name="\1\AO.Current.Units" Type="Str">Amps</Property>
				<Property Name="\1\AO.Max" Type="Str">0.02</Property>
				<Property Name="\1\AO.Min" Type="Str">0</Property>
				<Property Name="\1\AO.OutputType" Type="Str">Current</Property>
				<Property Name="\1\ChanType" Type="Str">Analog Output</Property>
				<Property Name="\1\Name" Type="Str">NI 9265 x2/CurrentOut_1</Property>
				<Property Name="\1\PhysicalChanName" Type="Str">cDAQ2Mod6/ao1</Property>
				<Property Name="\2\AO.Current.Units" Type="Str">Amps</Property>
				<Property Name="\2\AO.Max" Type="Str">0.02</Property>
				<Property Name="\2\AO.Min" Type="Str">0</Property>
				<Property Name="\2\AO.OutputType" Type="Str">Current</Property>
				<Property Name="\2\ChanType" Type="Str">Analog Output</Property>
				<Property Name="\2\Name" Type="Str">NI 9265 x2/CurrentOut_2</Property>
				<Property Name="\2\PhysicalChanName" Type="Str">cDAQ2Mod6/ao2</Property>
				<Property Name="\3\AO.Current.Units" Type="Str">Amps</Property>
				<Property Name="\3\AO.Max" Type="Str">0.02</Property>
				<Property Name="\3\AO.Min" Type="Str">0</Property>
				<Property Name="\3\AO.OutputType" Type="Str">Current</Property>
				<Property Name="\3\ChanType" Type="Str">Analog Output</Property>
				<Property Name="\3\Name" Type="Str">NI 9265 x2/CurrentOut_3</Property>
				<Property Name="\3\PhysicalChanName" Type="Str">cDAQ2Mod6/ao3</Property>
				<Property Name="Channels" Type="Str">NI 9265 x2/CurrentOut_0, NI 9265 x2/CurrentOut_1, NI 9265 x2/CurrentOut_2, NI 9265 x2/CurrentOut_3</Property>
				<Property Name="Name" Type="Str">NI 9265 x2</Property>
				<Property Name="RegenMode" Type="Str">Allow Regeneration</Property>
				<Property Name="SampClk.ActiveEdge" Type="Str">Rising</Property>
				<Property Name="SampClk.Rate" Type="Str">1000</Property>
				<Property Name="SampClk.Src" Type="Str"></Property>
				<Property Name="SampQuant.SampMode" Type="Str">Finite Samples</Property>
				<Property Name="SampQuant.SampPerChan" Type="Str">100</Property>
				<Property Name="SampTimingType" Type="Str">Sample Clock</Property>
			</Item>
			<Item Name="NI 9265 x3" Type="NI-DAQmx Task">
				<Property Name="\0\AO.Current.Units" Type="Str">Amps</Property>
				<Property Name="\0\AO.Max" Type="Str">0.02</Property>
				<Property Name="\0\AO.Min" Type="Str">0</Property>
				<Property Name="\0\AO.OutputType" Type="Str">Current</Property>
				<Property Name="\0\ChanType" Type="Str">Analog Output</Property>
				<Property Name="\0\Name" Type="Str">NI 9265 x3/CurrentOut_0</Property>
				<Property Name="\0\PhysicalChanName" Type="Str">cDAQ2Mod7/ao0</Property>
				<Property Name="\1\AO.Current.Units" Type="Str">Amps</Property>
				<Property Name="\1\AO.Max" Type="Str">0.02</Property>
				<Property Name="\1\AO.Min" Type="Str">0</Property>
				<Property Name="\1\AO.OutputType" Type="Str">Current</Property>
				<Property Name="\1\ChanType" Type="Str">Analog Output</Property>
				<Property Name="\1\Name" Type="Str">NI 9265 x3/CurrentOut_1</Property>
				<Property Name="\1\PhysicalChanName" Type="Str">cDAQ2Mod7/ao1</Property>
				<Property Name="\2\AO.Current.Units" Type="Str">Amps</Property>
				<Property Name="\2\AO.Max" Type="Str">0.02</Property>
				<Property Name="\2\AO.Min" Type="Str">0</Property>
				<Property Name="\2\AO.OutputType" Type="Str">Current</Property>
				<Property Name="\2\ChanType" Type="Str">Analog Output</Property>
				<Property Name="\2\Name" Type="Str">NI 9265 x3/CurrentOut_2</Property>
				<Property Name="\2\PhysicalChanName" Type="Str">cDAQ2Mod7/ao2</Property>
				<Property Name="\3\AO.Current.Units" Type="Str">Amps</Property>
				<Property Name="\3\AO.Max" Type="Str">0.02</Property>
				<Property Name="\3\AO.Min" Type="Str">0</Property>
				<Property Name="\3\AO.OutputType" Type="Str">Current</Property>
				<Property Name="\3\ChanType" Type="Str">Analog Output</Property>
				<Property Name="\3\Name" Type="Str">NI 9265 x3/CurrentOut_3</Property>
				<Property Name="\3\PhysicalChanName" Type="Str">cDAQ2Mod7/ao3</Property>
				<Property Name="Channels" Type="Str">NI 9265 x3/CurrentOut_0, NI 9265 x3/CurrentOut_1, NI 9265 x3/CurrentOut_2, NI 9265 x3/CurrentOut_3</Property>
				<Property Name="Name" Type="Str">NI 9265 x3</Property>
				<Property Name="RegenMode" Type="Str">Allow Regeneration</Property>
				<Property Name="SampClk.ActiveEdge" Type="Str">Rising</Property>
				<Property Name="SampClk.Rate" Type="Str">1000</Property>
				<Property Name="SampClk.Src" Type="Str"></Property>
				<Property Name="SampQuant.SampMode" Type="Str">Finite Samples</Property>
				<Property Name="SampQuant.SampPerChan" Type="Str">100</Property>
				<Property Name="SampTimingType" Type="Str">Sample Clock</Property>
			</Item>
			<Item Name="NI 9265 x4" Type="NI-DAQmx Task">
				<Property Name="\0\AO.Current.Units" Type="Str">Amps</Property>
				<Property Name="\0\AO.Max" Type="Str">0.02</Property>
				<Property Name="\0\AO.Min" Type="Str">0</Property>
				<Property Name="\0\AO.OutputType" Type="Str">Current</Property>
				<Property Name="\0\ChanType" Type="Str">Analog Output</Property>
				<Property Name="\0\Name" Type="Str">NI 9265 x4/CurrentOut_0</Property>
				<Property Name="\0\PhysicalChanName" Type="Str">cDAQ2Mod8/ao0</Property>
				<Property Name="\1\AO.Current.Units" Type="Str">Amps</Property>
				<Property Name="\1\AO.Max" Type="Str">0.02</Property>
				<Property Name="\1\AO.Min" Type="Str">0</Property>
				<Property Name="\1\AO.OutputType" Type="Str">Current</Property>
				<Property Name="\1\ChanType" Type="Str">Analog Output</Property>
				<Property Name="\1\Name" Type="Str">NI 9265 x4/CurrentOut_1</Property>
				<Property Name="\1\PhysicalChanName" Type="Str">cDAQ2Mod8/ao1</Property>
				<Property Name="\2\AO.Current.Units" Type="Str">Amps</Property>
				<Property Name="\2\AO.Max" Type="Str">0.02</Property>
				<Property Name="\2\AO.Min" Type="Str">0</Property>
				<Property Name="\2\AO.OutputType" Type="Str">Current</Property>
				<Property Name="\2\ChanType" Type="Str">Analog Output</Property>
				<Property Name="\2\Name" Type="Str">NI 9265 x4/CurrentOut_2</Property>
				<Property Name="\2\PhysicalChanName" Type="Str">cDAQ2Mod8/ao2</Property>
				<Property Name="\3\AO.Current.Units" Type="Str">Amps</Property>
				<Property Name="\3\AO.Max" Type="Str">0.02</Property>
				<Property Name="\3\AO.Min" Type="Str">0</Property>
				<Property Name="\3\AO.OutputType" Type="Str">Current</Property>
				<Property Name="\3\ChanType" Type="Str">Analog Output</Property>
				<Property Name="\3\Name" Type="Str">NI 9265 x4/CurrentOut_3</Property>
				<Property Name="\3\PhysicalChanName" Type="Str">cDAQ2Mod8/ao3</Property>
				<Property Name="Channels" Type="Str">NI 9265 x4/CurrentOut_0, NI 9265 x4/CurrentOut_1, NI 9265 x4/CurrentOut_2, NI 9265 x4/CurrentOut_3</Property>
				<Property Name="Name" Type="Str">NI 9265 x4</Property>
				<Property Name="RegenMode" Type="Str">Allow Regeneration</Property>
				<Property Name="SampClk.ActiveEdge" Type="Str">Rising</Property>
				<Property Name="SampClk.Rate" Type="Str">1000</Property>
				<Property Name="SampClk.Src" Type="Str"></Property>
				<Property Name="SampQuant.SampMode" Type="Str">Finite Samples</Property>
				<Property Name="SampQuant.SampPerChan" Type="Str">100</Property>
				<Property Name="SampTimingType" Type="Str">Sample Clock</Property>
			</Item>
			<Item Name="NI 9425" Type="NI-DAQmx Task">
				<Property Name="\0\ChanType" Type="Str">Digital Input</Property>
				<Property Name="\0\DI.InvertLines" Type="Str">0</Property>
				<Property Name="\0\Name" Type="Str">NI 9425/DigitalIn</Property>
				<Property Name="\0\PhysicalChanName" Type="Str">cDAQ2Mod3/port0</Property>
				<Property Name="Channels" Type="Str">NI 9425/DigitalIn</Property>
				<Property Name="Name" Type="Str">NI 9425</Property>
				<Property Name="SampTimingType" Type="Str">On Demand</Property>
			</Item>
			<Item Name="NI 9484" Type="NI-DAQmx Task">
				<Property Name="\0\ChanType" Type="Str">Digital Output</Property>
				<Property Name="\0\DO.InvertLines" Type="Str">0</Property>
				<Property Name="\0\Name" Type="Str">NI 9484/DigitalOut</Property>
				<Property Name="\0\PhysicalChanName" Type="Str">cDAQ2Mod4/port0</Property>
				<Property Name="Channels" Type="Str">NI 9484/DigitalOut</Property>
				<Property Name="Name" Type="Str">NI 9484</Property>
				<Property Name="SampTimingType" Type="Str">On Demand</Property>
			</Item>
		</Item>
		<Item Name="TCP" Type="Folder">
			<Item Name="main-client.vi" Type="VI" URL="../client/main-client.vi"/>
			<Item Name="Main-Server-win.vi" Type="VI" URL="../server/Main-Server-win.vi"/>
			<Item Name="TCP - Server.vi" Type="VI" URL="../server/TCP - Server.vi"/>
		</Item>
		<Item Name="xml" Type="Folder">
			<Item Name="XML read for cDAQ.vi" Type="VI" URL="../../xmlfile/XML read for cDAQ.vi"/>
			<Item Name="XML to Card.vi" Type="VI" URL="../../xmlfile/XML to Card.vi"/>
			<Item Name="xml read to cluster.vi" Type="VI" URL="../../xmlfile/xml read to cluster.vi"/>
			<Item Name="xml read to strings.vi" Type="VI" URL="../../xmlfile/xml read to strings.vi"/>
			<Item Name="xml read to strings cdaq.vi" Type="VI" URL="../../xmlfile/xml read to strings cdaq.vi"/>
			<Item Name="XML to Controller Info.vi" Type="VI" URL="../../xmlfile/XML to Controller Info.vi"/>
			<Item Name="xml read to cluster cdaq.vi" Type="VI" URL="../../xmlfile/xml read to cluster cdaq.vi"/>
			<Item Name="XML to File Info.vi" Type="VI" URL="../../xmlfile/XML to File Info.vi"/>
		</Item>
		<Item Name="setup daq tasks.vi" Type="VI" URL="../../xmlfile/setup daq tasks.vi"/>
		<Item Name="string to double.vi" Type="VI" URL="../helper vis/string to double.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="DAQmx Create AI Channel (sub).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create AI Channel (sub).vi"/>
				<Item Name="DAQmx Create AI Channel TEDS(sub).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create AI Channel TEDS(sub).vi"/>
				<Item Name="DAQmx Create AO Channel (sub).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create AO Channel (sub).vi"/>
				<Item Name="DAQmx Create Channel (AI-Acceleration-Accelerometer).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Acceleration-Accelerometer).vi"/>
				<Item Name="DAQmx Create Channel (AI-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (AI-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AI-Current-RMS).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Current-RMS).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (AI-Frequency-Voltage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Frequency-Voltage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-EddyCurrentProxProbe).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-EddyCurrentProxProbe).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-LVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-LVDT).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-RVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-RVDT).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Resistance).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Resistance).vi"/>
				<Item Name="DAQmx Create Channel (AI-Sound Pressure-Microphone).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Sound Pressure-Microphone).vi"/>
				<Item Name="DAQmx Create Channel (AI-Strain-Rosette Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Strain-Rosette Strain Gage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Strain-Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Strain-Strain Gage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Built-in Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Built-in Sensor).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-RTD).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-RTD).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermistor-Iex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermistor-Iex).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermistor-Vex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermistor-Vex).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermocouple).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermocouple).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Velocity-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Velocity-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-Custom with Excitation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-Custom with Excitation).vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-RMS).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-RMS).vi"/>
				<Item Name="DAQmx Create Channel (AO-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AO-FuncGen).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-FuncGen).vi"/>
				<Item Name="DAQmx Create Channel (AO-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Channel (CI-Count Edges).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Count Edges).vi"/>
				<Item Name="DAQmx Create Channel (CI-Frequency).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Frequency).vi"/>
				<Item Name="DAQmx Create Channel (CI-GPS Timestamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-GPS Timestamp).vi"/>
				<Item Name="DAQmx Create Channel (CI-Period).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Period).vi"/>
				<Item Name="DAQmx Create Channel (CI-Position-Angular Encoder).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Position-Angular Encoder).vi"/>
				<Item Name="DAQmx Create Channel (CI-Position-Linear Encoder).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Position-Linear Encoder).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Freq).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Freq).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Ticks).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Ticks).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Time).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Time).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Width).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Width).vi"/>
				<Item Name="DAQmx Create Channel (CI-Semi Period).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Semi Period).vi"/>
				<Item Name="DAQmx Create Channel (CI-Two Edge Separation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Two Edge Separation).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Frequency).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Frequency).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Ticks).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Ticks).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Time).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Time).vi"/>
				<Item Name="DAQmx Create Channel (DI-Digital Input).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (DI-Digital Input).vi"/>
				<Item Name="DAQmx Create Channel (DO-Digital Output).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (DO-Digital Output).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Acceleration-Accelerometer).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Acceleration-Accelerometer).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Force-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Force-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Force-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Force-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Position-LVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Position-LVDT).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Position-RVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Position-RVDT).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Pressure-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Pressure-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Resistance).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Resistance).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Sound Pressure-Microphone).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Sound Pressure-Microphone).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Strain-Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Strain-Strain Gage).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-RTD).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-RTD).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Iex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Iex).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Vex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Vex).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermocouple).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermocouple).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Torque-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Torque-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Voltage-Custom with Excitation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Voltage-Custom with Excitation).vi"/>
				<Item Name="DAQmx Create CI Channel (sub).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create CI Channel (sub).vi"/>
				<Item Name="DAQmx Create CO Channel (sub).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create CO Channel (sub).vi"/>
				<Item Name="DAQmx Create DI Channel (sub).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create DI Channel (sub).vi"/>
				<Item Name="DAQmx Create DO Channel (sub).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create DO Channel (sub).vi"/>
				<Item Name="DAQmx Create Strain Rosette AI Channels (sub).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Strain Rosette AI Channels (sub).vi"/>
				<Item Name="DAQmx Create Virtual Channel.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Virtual Channel.vi"/>
				<Item Name="DAQmx Fill In Error Info.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/miscellaneous.llb/DAQmx Fill In Error Info.vi"/>
				<Item Name="DAQmx Read (Analog 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D I16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D I16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D I32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D I32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Freq 1 Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Freq 1 Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Ticks 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Ticks 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Time 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Time 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Freq 1 Chan 1 Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Freq 1 Chan 1 Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Ticks 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Ticks 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Time 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Time 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Bool 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Bool 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Bool NChan 1Samp 1Line).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Bool NChan 1Samp 1Line).vi"/>
				<Item Name="DAQmx Read (Digital 1D U8 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U8 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U8 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U8 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U16 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U16 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U16 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U16 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 2D Bool NChan 1Samp NLine).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D Bool NChan 1Samp NLine).vi"/>
				<Item Name="DAQmx Read (Digital 2D U8 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U8 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital Bool 1Line 1Point).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Bool 1Line 1Point).vi"/>
				<Item Name="DAQmx Read (Digital U8 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U8 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital U16 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U16 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Raw 1D I8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I8).vi"/>
				<Item Name="DAQmx Read (Raw 1D I16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I16).vi"/>
				<Item Name="DAQmx Read (Raw 1D I32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I32).vi"/>
				<Item Name="DAQmx Read (Raw 1D U8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U8).vi"/>
				<Item Name="DAQmx Read (Raw 1D U16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U16).vi"/>
				<Item Name="DAQmx Read (Raw 1D U32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U32).vi"/>
				<Item Name="DAQmx Read.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read.vi"/>
				<Item Name="DAQmx Rollback Channel If Error.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Rollback Channel If Error.vi"/>
				<Item Name="DAQmx Set CJC Parameters (sub).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Set CJC Parameters (sub).vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="GXML.lvlib" Type="Library" URL="/&lt;vilib&gt;/NI/GXML/GXML.lvlib"/>
				<Item Name="Internecine Avoider.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/Internecine Avoider.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVNumericRepresentation.ctl" Type="VI" URL="/&lt;vilib&gt;/numeric/LVNumericRepresentation.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="NI_XML.lvlib" Type="Library" URL="/&lt;vilib&gt;/xml/NI_XML.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="TCP Listen Internal List.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/TCP Listen Internal List.vi"/>
				<Item Name="TCP Listen List Operations.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/TCP Listen List Operations.ctl"/>
				<Item Name="TCP Listen.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/TCP Listen.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="VariantType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/VariantDataType/VariantType.lvlib"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="DOMUserDefRef.dll" Type="Document" URL="DOMUserDefRef.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="nilvaiu.dll" Type="Document" URL="nilvaiu.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
	<Item Name="RT CompactDAQ Target" Type="RT CDAQ Chassis">
		<Property Name="alias.name" Type="Str">RT CompactDAQ Target</Property>
		<Property Name="alias.value" Type="Str">0.0.0.0</Property>
		<Property Name="CCSymbols" Type="Str">TARGET_TYPE,RT;OS,PharLap;CPU,x86;</Property>
		<Property Name="host.ResponsivenessCheckEnabled" Type="Bool">true</Property>
		<Property Name="host.ResponsivenessCheckPingDelay" Type="UInt">5000</Property>
		<Property Name="host.ResponsivenessCheckPingTimeout" Type="UInt">1000</Property>
		<Property Name="host.TargetCPUID" Type="UInt">3</Property>
		<Property Name="host.TargetOSID" Type="UInt">15</Property>
		<Property Name="target.cleanupVisa" Type="Bool">false</Property>
		<Property Name="target.FPProtocolGlobals_ControlTimeLimit" Type="Int">300</Property>
		<Property Name="target.getDefault-&gt;WebServer.Port" Type="Int">80</Property>
		<Property Name="target.getDefault-&gt;WebServer.Timeout" Type="Int">60</Property>
		<Property Name="target.IOScan.Faults" Type="Str"></Property>
		<Property Name="target.IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="target.IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="target.IOScan.Period" Type="UInt">10000</Property>
		<Property Name="target.IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="target.IOScan.Priority" Type="UInt">0</Property>
		<Property Name="target.IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="target.IsRemotePanelSupported" Type="Bool">true</Property>
		<Property Name="target.RTCPULoadMonitoringEnabled" Type="Bool">true</Property>
		<Property Name="target.RTDebugWebServerHTTPPort" Type="Int">8001</Property>
		<Property Name="target.RTTarget.ApplicationPath" Type="Path">/c/ni-rt/startup/startup.rtexe</Property>
		<Property Name="target.RTTarget.EnableFileSharing" Type="Bool">true</Property>
		<Property Name="target.RTTarget.IPAccess" Type="Str">+*</Property>
		<Property Name="target.RTTarget.LaunchAppAtBoot" Type="Bool">false</Property>
		<Property Name="target.RTTarget.VIPath" Type="Path">/c/ni-rt/startup</Property>
		<Property Name="target.server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.tcp.access" Type="Str">+*</Property>
		<Property Name="target.server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="target.server.tcp.paranoid" Type="Bool">true</Property>
		<Property Name="target.server.tcp.port" Type="Int">3363</Property>
		<Property Name="target.server.tcp.serviceName" Type="Str">Main Application Instance/VI Server</Property>
		<Property Name="target.server.tcp.serviceName.default" Type="Str">Main Application Instance/VI Server</Property>
		<Property Name="target.server.vi.access" Type="Str">+*</Property>
		<Property Name="target.server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="target.server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.WebServer.Enabled" Type="Bool">false</Property>
		<Property Name="target.WebServer.LogEnabled" Type="Bool">false</Property>
		<Property Name="target.WebServer.LogPath" Type="Path">/c/ni-rt/system/www/www.log</Property>
		<Property Name="target.WebServer.Port" Type="Int">80</Property>
		<Property Name="target.WebServer.RootPath" Type="Path">/c/ni-rt/system/www</Property>
		<Property Name="target.WebServer.TcpAccess" Type="Str">c+*</Property>
		<Property Name="target.WebServer.Timeout" Type="Int">60</Property>
		<Property Name="target.WebServer.ViAccess" Type="Str">+*</Property>
		<Property Name="target.webservices.SecurityAPIKey" Type="Str">PqVr/ifkAQh+lVrdPIykXlFvg12GhhQFR8H9cUhphgg=:pTe9HRlQuMfJxAG6QCGq7UvoUpJzAzWGKy5SbZ+roSU=</Property>
		<Property Name="target.webservices.ValidTimestampWindow" Type="Int">15</Property>
		<Item Name="Dependencies" Type="Dependencies"/>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
